﻿using CheckSignal.Back;
using CheckSignal.Back.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckSignal
{
  public partial class formAdding : Form
  {
    public int IDD;
    public string Namee;
    public string IPP;
    public string Portt;
    public string Topicc;
    public string Projectt;
    public string Gmaill;
    public Client client = new Client();
    private List<Factory> facts = new List<Factory>();
    public formAdding()
    {
      InitializeComponent();


    }
    private Task<List<Factory>> LoadFact()
    {
      return Task<List<Factory>>.Run(() =>
      {
        using (var db = new MQTTContext())
        {
          return db.Factory.ToList();
        }

      });
    }
    private bool IsIP(string value)
    {
      var Pattern = new string[]
        {
            "^",                                            // Start of string
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.",    // Between 000 and 255 and "."
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.",
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.",
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])",      // Same as before, no period
            "$",                                            // End of string
        };

      // Evaluates to true 
      return Regex.IsMatch(value, string.Join(string.Empty, Pattern));
    }
    private void btnAdd_Click(object sender, EventArgs e)
    {


      if (!IsIP(txtHostenter.Text))
      {
        MessageBox.Show("IP Wrong Format!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (!int.TryParse(txtPortenter.Text, out int ret))
      {
        MessageBox.Show("Port Wrong Format!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (txtName.Text != "" && txtTopic.Text != "" && txtGmail.Text != "")
      {
        client = new Client()
        {
          Name = txtName.Text,
          HostIP = txtHostenter.Text,
          HostPort = txtPortenter.Text,
          Topic = txtTopic.Text,
          Gmail = txtGmail.Text,
          FactoryId = comboProject.SelectedIndex + 1,
          Project = comboProject.SelectedItem.ToString()
      };
        Namee = txtName.Text;
        IPP = txtHostenter.Text;
        Portt = txtPortenter.Text;
        Topicc = txtTopic.Text;
        Projectt = comboProject.SelectedItem.ToString();
        Gmaill = txtGmail.Text;

        this.DialogResult = DialogResult.OK;
      }
      else MessageBox.Show("Please enter all infomation of factory", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {

      this.DialogResult = DialogResult.Cancel;
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      txtHostenter.Text = "";

      txtName.Text = "";
    }

    private void textBox1_TextChanged(object sender, EventArgs e)
    {

    }

    private async void formAdding_Load(object sender, EventArgs e)
    {
      facts = await LoadFact();
      comboProject.Items.Clear();
      comboProject.Items.AddRange(facts.Select(s => s.Name).ToArray());
    }
  }
}

