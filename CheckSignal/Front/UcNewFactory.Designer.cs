﻿
namespace CheckSignal
{
    partial class UcNewFactory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.tableInfo = new System.Windows.Forms.TableLayoutPanel();
      this.lblIP = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lblID = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.lblLastPublic = new System.Windows.Forms.Label();
      this.lblLastRecv = new System.Windows.Forms.Label();
      this.lblTimedelay = new System.Windows.Forms.Label();
      this.lblLastDisconnect = new System.Windows.Forms.Label();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.layoutStatus = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.txtProject = new System.Windows.Forms.Label();
      this.lblName = new System.Windows.Forms.Label();
      this.lblStatus = new System.Windows.Forms.Panel();
      this.btnConnecting = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.btnDisconnecting = new System.Windows.Forms.Button();
      this.btnSetting = new System.Windows.Forms.Button();
      this.panelsetting = new System.Windows.Forms.Panel();
      this.tableInfo.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.layoutStatus.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.panelsetting.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableInfo
      // 
      this.tableInfo.ColumnCount = 2;
      this.tableInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.65394F));
      this.tableInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.34606F));
      this.tableInfo.Controls.Add(this.lblIP, 0, 0);
      this.tableInfo.Controls.Add(this.label2, 0, 1);
      this.tableInfo.Controls.Add(this.lblID, 1, 0);
      this.tableInfo.Controls.Add(this.label3, 0, 2);
      this.tableInfo.Controls.Add(this.label4, 0, 3);
      this.tableInfo.Controls.Add(this.label5, 0, 4);
      this.tableInfo.Controls.Add(this.lblLastPublic, 1, 1);
      this.tableInfo.Controls.Add(this.lblLastRecv, 1, 2);
      this.tableInfo.Controls.Add(this.lblTimedelay, 1, 3);
      this.tableInfo.Controls.Add(this.lblLastDisconnect, 1, 4);
      this.tableInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableInfo.Location = new System.Drawing.Point(4, 71);
      this.tableInfo.Name = "tableInfo";
      this.tableInfo.RowCount = 5;
      this.tableInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableInfo.Size = new System.Drawing.Size(376, 249);
      this.tableInfo.TabIndex = 0;
      // 
      // lblIP
      // 
      this.lblIP.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.lblIP.AutoSize = true;
      this.lblIP.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
      this.lblIP.Location = new System.Drawing.Point(3, 14);
      this.lblIP.Name = "lblIP";
      this.lblIP.Size = new System.Drawing.Size(183, 20);
      this.lblIP.TabIndex = 0;
      this.lblIP.Text = "Host: 192.168.1.10:1883";
      // 
      // label2
      // 
      this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 63);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(119, 20);
      this.label2.TabIndex = 0;
      this.label2.Text = "Last Public Time:";
      // 
      // lblID
      // 
      this.lblID.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.lblID.AutoSize = true;
      this.lblID.Location = new System.Drawing.Point(346, 14);
      this.lblID.Name = "lblID";
      this.lblID.Size = new System.Drawing.Size(27, 20);
      this.lblID.TabIndex = 0;
      this.lblID.Text = "ID:";
      // 
      // label3
      // 
      this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 112);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(130, 20);
      this.label3.TabIndex = 0;
      this.label3.Text = "Last Receive Time:";
      // 
      // label4
      // 
      this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(3, 161);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(142, 20);
      this.label4.TabIndex = 0;
      this.label4.Text = "Time Receive Delay:";
      // 
      // label5
      // 
      this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(3, 212);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(152, 20);
      this.label5.TabIndex = 0;
      this.label5.Text = "Last Disconnect Time:";
      // 
      // lblLastPublic
      // 
      this.lblLastPublic.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.lblLastPublic.AutoSize = true;
      this.lblLastPublic.Location = new System.Drawing.Point(355, 63);
      this.lblLastPublic.Name = "lblLastPublic";
      this.lblLastPublic.Size = new System.Drawing.Size(18, 20);
      this.lblLastPublic.TabIndex = 0;
      this.lblLastPublic.Text = "...";
      // 
      // lblLastRecv
      // 
      this.lblLastRecv.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.lblLastRecv.AutoSize = true;
      this.lblLastRecv.Location = new System.Drawing.Point(355, 112);
      this.lblLastRecv.Name = "lblLastRecv";
      this.lblLastRecv.Size = new System.Drawing.Size(18, 20);
      this.lblLastRecv.TabIndex = 0;
      this.lblLastRecv.Text = "...";
      // 
      // lblTimedelay
      // 
      this.lblTimedelay.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.lblTimedelay.AutoSize = true;
      this.lblTimedelay.Location = new System.Drawing.Point(355, 161);
      this.lblTimedelay.Name = "lblTimedelay";
      this.lblTimedelay.Size = new System.Drawing.Size(18, 20);
      this.lblTimedelay.TabIndex = 0;
      this.lblTimedelay.Text = "...";
      // 
      // lblLastDisconnect
      // 
      this.lblLastDisconnect.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.lblLastDisconnect.AutoSize = true;
      this.lblLastDisconnect.Location = new System.Drawing.Point(355, 212);
      this.lblLastDisconnect.Name = "lblLastDisconnect";
      this.lblLastDisconnect.Size = new System.Drawing.Size(18, 20);
      this.lblLastDisconnect.TabIndex = 0;
      this.lblLastDisconnect.Text = "...";
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Controls.Add(this.tableInfo, 0, 1);
      this.tableLayoutPanel2.Controls.Add(this.layoutStatus, 0, 0);
      this.tableLayoutPanel2.Location = new System.Drawing.Point(13, 12);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.67901F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.32098F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(384, 324);
      this.tableLayoutPanel2.TabIndex = 1;
      // 
      // layoutStatus
      // 
      this.layoutStatus.BackColor = System.Drawing.SystemColors.HotTrack;
      this.layoutStatus.ColumnCount = 2;
      this.layoutStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.09756F));
      this.layoutStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.90244F));
      this.layoutStatus.Controls.Add(this.tableLayoutPanel1, 0, 0);
      this.layoutStatus.Controls.Add(this.lblStatus, 1, 0);
      this.layoutStatus.Dock = System.Windows.Forms.DockStyle.Fill;
      this.layoutStatus.Location = new System.Drawing.Point(4, 4);
      this.layoutStatus.Name = "layoutStatus";
      this.layoutStatus.RowCount = 1;
      this.layoutStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.layoutStatus.Size = new System.Drawing.Size(376, 60);
      this.layoutStatus.TabIndex = 1;
      this.layoutStatus.Click += new System.EventHandler(this.lblStatus_Click);
      this.layoutStatus.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tableLayoutPanel3_MouseClick);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.txtProject, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.lblName, 0, 0);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(203, 53);
      this.tableLayoutPanel1.TabIndex = 7;
      // 
      // txtProject
      // 
      this.txtProject.AutoSize = true;
      this.txtProject.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
      this.txtProject.ForeColor = System.Drawing.SystemColors.ButtonFace;
      this.txtProject.Location = new System.Drawing.Point(3, 35);
      this.txtProject.Name = "txtProject";
      this.txtProject.Size = new System.Drawing.Size(54, 18);
      this.txtProject.TabIndex = 7;
      this.txtProject.Text = "Project";
      this.txtProject.Click += new System.EventHandler(this.txtProject_Click);
      // 
      // lblName
      // 
      this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.lblName.AutoSize = true;
      this.lblName.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
      this.lblName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.lblName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lblName.Location = new System.Drawing.Point(3, 0);
      this.lblName.Name = "lblName";
      this.lblName.Size = new System.Drawing.Size(178, 35);
      this.lblName.TabIndex = 0;
      this.lblName.Text = "Factory Name";
      this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lblName.Click += new System.EventHandler(this.lblStatus_Click);
      // 
      // lblStatus
      // 
      this.lblStatus.BackgroundImage = global::CheckSignal.Properties.Resources.dissconnect;
      this.lblStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.lblStatus.Location = new System.Drawing.Point(213, 3);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(158, 53);
      this.lblStatus.TabIndex = 1;
      this.lblStatus.Click += new System.EventHandler(this.lblStatus_Click);
      // 
      // btnConnecting
      // 
      this.btnConnecting.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.btnConnecting.BackgroundImage = global::CheckSignal.Properties.Resources.connecting;
      this.btnConnecting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btnConnecting.Location = new System.Drawing.Point(3, 3);
      this.btnConnecting.Name = "btnConnecting";
      this.btnConnecting.Size = new System.Drawing.Size(81, 34);
      this.btnConnecting.TabIndex = 2;
      this.btnConnecting.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btnConnecting.UseVisualStyleBackColor = true;
      this.btnConnecting.Click += new System.EventHandler(this.btnConnecting_Click);
      // 
      // btnExit
      // 
      this.btnExit.BackColor = System.Drawing.Color.Transparent;
      this.btnExit.BackgroundImage = global::CheckSignal.Properties.Resources.exit;
      this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btnExit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
      this.btnExit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.btnExit.Location = new System.Drawing.Point(344, 3);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(35, 35);
      this.btnExit.TabIndex = 3;
      this.btnExit.UseVisualStyleBackColor = false;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // btnDisconnecting
      // 
      this.btnDisconnecting.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.btnDisconnecting.BackgroundImage = global::CheckSignal.Properties.Resources.dissconnecting;
      this.btnDisconnecting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btnDisconnecting.Location = new System.Drawing.Point(90, 3);
      this.btnDisconnecting.Name = "btnDisconnecting";
      this.btnDisconnecting.Size = new System.Drawing.Size(81, 34);
      this.btnDisconnecting.TabIndex = 4;
      this.btnDisconnecting.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btnDisconnecting.UseVisualStyleBackColor = true;
      this.btnDisconnecting.Click += new System.EventHandler(this.btnDisconnecting_Click);
      // 
      // btnSetting
      // 
      this.btnSetting.BackColor = System.Drawing.Color.Transparent;
      this.btnSetting.BackgroundImage = global::CheckSignal.Properties.Resources.setting;
      this.btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btnSetting.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
      this.btnSetting.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.btnSetting.Location = new System.Drawing.Point(298, 3);
      this.btnSetting.Name = "btnSetting";
      this.btnSetting.Size = new System.Drawing.Size(40, 35);
      this.btnSetting.TabIndex = 5;
      this.btnSetting.UseVisualStyleBackColor = false;
      this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
      // 
      // panelsetting
      // 
      this.panelsetting.Controls.Add(this.btnDisconnecting);
      this.panelsetting.Controls.Add(this.btnSetting);
      this.panelsetting.Controls.Add(this.btnConnecting);
      this.panelsetting.Controls.Add(this.btnExit);
      this.panelsetting.Location = new System.Drawing.Point(13, 341);
      this.panelsetting.Name = "panelsetting";
      this.panelsetting.Size = new System.Drawing.Size(384, 43);
      this.panelsetting.TabIndex = 6;
      this.panelsetting.Visible = false;
      // 
      // UcNewFactory
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.Controls.Add(this.panelsetting);
      this.Controls.Add(this.tableLayoutPanel2);
      this.MaximumSize = new System.Drawing.Size(411, 401);
      this.MinimumSize = new System.Drawing.Size(410, 89);
      this.Name = "UcNewFactory";
      this.Size = new System.Drawing.Size(411, 401);
      this.Load += new System.EventHandler(this.UcNewFactory_Load);
      this.tableInfo.ResumeLayout(false);
      this.tableInfo.PerformLayout();
      this.tableLayoutPanel2.ResumeLayout(false);
      this.layoutStatus.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.panelsetting.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel layoutStatus;
        private System.Windows.Forms.Button btnConnecting;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnDisconnecting;
        private System.Windows.Forms.Panel lblStatus;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblLastPublic;
        private System.Windows.Forms.Label lblLastRecv;
        private System.Windows.Forms.Label lblTimedelay;
        private System.Windows.Forms.Label lblLastDisconnect;
        private System.Windows.Forms.Panel panelsetting;
        public System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label txtProject;
    }
}
