﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckSignal.Front
{
  public partial class FrmNotify : Form
  {
    public string Message
    {
      get
      {
        string ret = "";
        if (this.InvokeRequired)
        {
          ret = this.label1.Text;
        }
        else
        {
          ret = this.label1.Text;
        }
        return ret;
      }
      set
      {
        if (this.InvokeRequired)
        {
          this.label1.Text = value;
        }
        else
        {
          this.label1.Text = value;
        }

      }
    }
    public FrmNotify()
    {
      InitializeComponent();
    }
  }
}
