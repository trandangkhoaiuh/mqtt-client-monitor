﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Formatter;
using System.Net;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client;
using MQTTnet.Client.Subscribing;
using System.Text.Json;
using System.Net.Mail;
using System.Net.Mime;
using System.ComponentModel;
using CheckSignal.Back;
using CheckSignal.Back.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace CheckSignal
{
  public partial class UcNewFactory : UserControl
  {
    public UcNewFactory()
    {
      InitializeComponent();
    }
    private IMqttClient mqttClient1;
    private IMqttClientOptions options1;
    private int IDhost;
    public Factoryinfo factoryInfo { get; internal set; }
    public Client ClientInfo { get; internal set; }
    public FormHome frmParent { get; internal set; }

    public delegate void CloseHandler(object sender, int clientID);
    public event CloseHandler CloseEvent;

    FormHome newformhome = new FormHome();
    private void UcNewFactory_Load(object sender, EventArgs e)
    {
      //string[] hostt = factoryInfo.Host.Split(":");
      //connecting(hostt[0], Int32.Parse(hostt[1]), factoryInfo.Topic);
      connecting(ClientInfo.HostIP, int.Parse(ClientInfo.HostPort), ClientInfo.Topic);
      IDhost = ClientInfo.Id;

      this.Disposed += UcNewFactory_Disposed;
    }

    private void UcNewFactory_Disposed(object sender, EventArgs e)
    {
      mqttClient1.Dispose();
    }

    public async void connecting(string ip, int port, string topic)
    {
      formatSystemConnect(0);
      var factory = new MqttFactory();
      mqttClient1 = factory.CreateMqttClient();

      options1 = new MqttClientOptionsBuilder()
        //.WithCommunicationTimeout(TimeSpan.FromMilliseconds(2000))
         .WithTcpServer(ip, port)
         .WithClientId(ClientInfo.Id.ToString())
         .Build();

      mqttClient1.UseConnectedHandler(async e =>
      {
        await mqttClient1.UnsubscribeAsync(new string[] { topic });
        await mqttClient1.SubscribeAsync(new MqttClientSubscribeOptionsBuilder().WithTopicFilter(topic).Build());
        formatSystemConnect(1);
      });
      mqttClient1.UseApplicationMessageReceivedHandler(Recv1);
      mqttClient1.UseDisconnectedHandler(Disconect1);
      try
      {
        await mqttClient1.ConnectAsync(options1, CancellationToken.None);
      }
      catch (Exception ex) 
      { 
        //formatSystemConnect(2); 
      }

    }

    private async void Disconect1(MqttClientDisconnectedEventArgs arg)
    {
      try
      {
        Convert_CrossThread(lblLastDisconnect, DateTime.Now.ToString("HH:mm:ss tt"));

        if (!this.IsDisposed)
        {
          if (this != null)
          {
            formatSystemConnect(2);
          }

        }
        await Task.Run(() =>
        {
          Task.Delay(5000).Wait();
        });
        //mqttClient1.ReconnectAsync();
      }
      catch (Exception ex)
      {

        Console.WriteLine(ex.Message);
      }

      //return Task.Run(() => MessageBox.Show(factoryInfo.Host.ToString() + " Disconnectd"));
    }

    private Task Recv1(MqttApplicationMessageReceivedEventArgs arg)
    {
      //{ "lastPub":"a","lastDiss":"b","payload":"c", "ID":1}
      if (arg.ApplicationMessage.Payload != null)
      {
        var paload = Encoding.UTF8.GetString(arg.ApplicationMessage.Payload);
        if (paload.Length > 33)
        {
          var factoryrecv = JsonSerializer.Deserialize<Factoryinfo>(paload);
          if (IDhost == factoryrecv.ClientId)
          {
            loadData(factoryrecv.Date, factoryrecv.ConnStatus);
          }
        }
      }

      return Task.Run(() => Console.WriteLine("ok"));
    }
    public void loadDatanew()
    {
      Convert_CrossThread(lblName, ClientInfo.Name);
      Convert_CrossThread(lblIP, ClientInfo.HostIP);
      Convert_CrossThread(lblID, "ID: " + ClientInfo.Id.ToString());
      Convert_CrossThread(txtProject, "Project: " + ClientInfo.Project);
    }

    public void loadData(double lastpub, int error)
    {
      double timenow = DateTimeOffset.Now.ToUnixTimeSeconds();
      Convert_CrossThread(lblLastPublic, UnixTimeStampToDateTime(lastpub).ToString("HH:mm:ss tt"));
      Convert_CrossThread(lblLastRecv, DateTime.Now.ToString("HH:mm:ss tt"));
      Convert_CrossThread(lblTimedelay, UnixTimeStampToDateTime(timenow - ClientInfo.Date).ToString("HH:mm:ss"));

      //set status
      if (error == 0)
      {
        layoutStatus.Invoke((MethodInvoker)delegate ()
        {
          layoutStatus.BackColor = SystemColors.HotTrack;
        });
        lblStatus.Invoke((MethodInvoker)delegate ()
        {
          lblStatus.BackgroundImage = Properties.Resources.dissconnect;
        });
        Convert_CrossThread(lblLastDisconnect, DateTime.Now.ToString("HH:mm:ss tt"));
        //sentgmail(factoryInfo.Gmail, factoryInfo.FactoryName + " with Host: " +  factoryInfo.Host + " Disconnected");
      }
      else if (error == 1)
      {
        layoutStatus.Invoke((MethodInvoker)delegate ()
        {
          layoutStatus.BackColor = Color.Green;
        });
        lblStatus.Invoke((MethodInvoker)delegate ()
        {
          lblStatus.BackgroundImage = Properties.Resources.connect;
        });

      }
      else
      {
        layoutStatus.Invoke((MethodInvoker)delegate ()
        {
          layoutStatus.BackColor = Color.Red;
        });
        lblStatus.Invoke((MethodInvoker)delegate ()
        {
          lblStatus.BackgroundImage = Properties.Resources.error;
        });
        Convert_CrossThread(lblLastDisconnect, DateTime.Now.ToString("HH:mm:ss tt"));
        string mess = "Factory: "+ ClientInfo.Name + " (in Project: " + ClientInfo.Project + ")"+ "\r\n" + "Host: " + ClientInfo.HostIP + ":"+ ClientInfo.HostPort+ "\r\n" + "Error, let's check it!!";
        sentgmail(ClientInfo.Gmail, mess);

      }
    }


    public void Convert_CrossThread(Label label, string value)
    {
      if (label.InvokeRequired)
      {
        label.Invoke((MethodInvoker)delegate ()
        {
          label.Text = value;
        });
      }
      else
      {
        label.Text = value;
      }
    }

    private void btnConnecting_Click(object sender, EventArgs e)
    {
      //string[] hostt = factoryInfo.Host.Split(":");
      //connecting(hostt[0], Int32.Parse(hostt[1]), factoryInfo.Topic);
      connecting(ClientInfo.HostIP, int.Parse(ClientInfo.HostPort), ClientInfo.Topic);
      IDhost = ClientInfo.Id;
    }
    private void ConnectToHost()
    {

    }
    private void btnDisconnecting_Click(object sender, EventArgs e)
    {
      mqttClient1.DisconnectAsync();
    }

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
      // Unix timestamp is seconds past epoch
      System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
      dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
      return dtDateTime;
    }

    private void tableLayoutPanel3_MouseClick(object sender, MouseEventArgs e)
    {

      panelsetting.Visible = !panelsetting.Visible;
    }



    private void btnExit_Click(object sender, EventArgs e)
    {
      var resq = MessageBox.Show("Are you sure you want to delete this client?" + Environment.NewLine + "This action will delete the client in the database!", "Delete Client",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
      if (resq== DialogResult.Yes )
      {
        this.CloseEvent(this, IDhost);
        DeleteClient(ClientInfo.Id);
      }

    }

    private void btnSetting_Click(object sender, EventArgs e)
    {

      formAdding add = new formAdding();
      var edit = add.ShowDialog();
      if (edit == DialogResult.OK)
      {
        ClientInfo.Name = add.Namee;
        ClientInfo.HostIP = add.IPP;
        ClientInfo.HostPort = add.Portt;
        ClientInfo.Topic = add.Topicc;
        ClientInfo.Gmail = add.Gmaill;
        ClientInfo.Project = add.Projectt;

        loadDatanew();
        connecting(ClientInfo.HostIP, int.Parse(ClientInfo.HostPort), ClientInfo.Topic);
        IDhost = ClientInfo.Id;

        EditClient(ClientInfo.Id, ClientInfo.Name, ClientInfo.HostIP, ClientInfo.HostPort, ClientInfo.Topic, ClientInfo.Gmail, ClientInfo.Project);



      }
    }

    public static async Task EditClient(int id, string newName, string newIP, string newPort, string newTopic, string newGmail, string newProject)
    {
      using (var context = new MQTTContext())
      {
        //get factory from ID 
        var factory = await (from p in context.Client where (p.Id == id) select p).FirstOrDefaultAsync();

        if (factory != null)
        {
          factory.Name = newName;
          factory.HostIP = newIP;
          factory.HostPort = newPort;
          factory.Topic = newTopic;
          factory.Gmail = newGmail;
          factory.Project = newProject;
          await context.SaveChangesAsync();
        }
      }
    }

    public static async Task DeleteClient(int id)
    {
      using (var context = new MQTTContext())
      {
        //get factory from ID 
        var factory = await (from p in context.Client where (p.Id == id) select p).FirstOrDefaultAsync();

        if (factory != null)
        {
          context.Remove(factory);
          await context.SaveChangesAsync();
        }
      }
    }

    void formatSystemConnect(int status)
    {
      /*if (mqttClient1.IsConnected)
        status = 1;
      else
      {
        status = 2;
      }*/
      if (status == 1)
      {
        tableInfo.Invoke((MethodInvoker)delegate ()
        {
          tableInfo.BackColor = Color.Transparent;
        });
        lblStatus?.BeginInvoke((MethodInvoker)delegate ()
        {
          lblStatus.BackgroundImage = Properties.Resources.connect;
        });
        layoutStatus?.BeginInvoke((MethodInvoker)delegate ()
        {
          layoutStatus.BackColor = Color.Green;
        });
      }
      else if (status == 0)
      {
        tableInfo.Invoke((MethodInvoker)delegate ()
        {
          tableInfo.BackColor = SystemColors.HotTrack;
        });
        lblStatus?.BeginInvoke((MethodInvoker)delegate ()
        {
          lblStatus.BackgroundImage = Properties.Resources.dissconnect;
        });
        layoutStatus?.BeginInvoke((MethodInvoker)delegate ()
        {
          layoutStatus.BackColor = SystemColors.HotTrack;
        });
      }
      else
      {
        layoutStatus?.BeginInvoke((MethodInvoker)delegate ()
        {
          layoutStatus.BackColor = Color.Red;
        });
        lblStatus?.BeginInvoke((MethodInvoker)delegate ()
        {
          lblStatus.BackgroundImage = Properties.Resources.error;
        });
        Convert_CrossThread(lblLastDisconnect, DateTime.Now.ToString("HH:mm:ss tt"));

        tableInfo?.BeginInvoke((MethodInvoker)delegate ()
        {
          tableInfo.BackColor = Color.Red;
        });



      }
    }

    private void lblStatus_Click(object sender, EventArgs e)
    {
      if (this.Size == MaximumSize)
      {
        this.Size = MinimumSize;
        tableLayoutPanel2.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
      }
      else
      {
        this.Size = MaximumSize;
        tableLayoutPanel2.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
      }
    }

    private void txtProject_Click(object sender, EventArgs e)
    {
      this.Size = MaximumSize;
      tableLayoutPanel2.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
      panelsetting.Visible = !panelsetting.Visible;
    }

    void sentgmail(string mail, string mess)
    {
      var smtpClient = new SmtpClient("smtp.gmail.com")
      {
        Port = 587,
        Credentials = new NetworkCredential("vuletechdangkhoa@gmail.com", "ABC1234567890!@"),
        EnableSsl = true,
      };
      if (mail != null)
      {
        try
        {
          smtpClient.Send("vuletechdangkhoa@gmail.com", mail, "iMag notification", mess);
        }
        catch
        {

        }
      }

    }
  }
}
