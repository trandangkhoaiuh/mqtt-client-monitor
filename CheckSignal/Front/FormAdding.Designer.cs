﻿
namespace CheckSignal
{
    partial class formAdding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.label4 = new System.Windows.Forms.Label();
      this.txtTopic = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.txtHostenter = new System.Windows.Forms.TextBox();
      this.txtPortenter = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.label3 = new System.Windows.Forms.Label();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.comboProject = new System.Windows.Forms.ComboBox();
      this.btnAdd = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnReset = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.txtGmail = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel4.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 2);
      this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
      this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
      this.tableLayoutPanel2.Location = new System.Drawing.Point(11, 12);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 3;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.85965F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.14035F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(425, 141);
      this.tableLayoutPanel2.TabIndex = 2;
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.BackColor = System.Drawing.SystemColors.Control;
      this.tableLayoutPanel4.ColumnCount = 2;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.55847F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.44153F));
      this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
      this.tableLayoutPanel4.Controls.Add(this.txtTopic, 1, 0);
      this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 106);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 1;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(419, 32);
      this.tableLayoutPanel4.TabIndex = 2;
      // 
      // label4
      // 
      this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(3, 6);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(48, 20);
      this.label4.TabIndex = 5;
      this.label4.Text = "Topic:";
      // 
      // txtTopic
      // 
      this.txtTopic.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.txtTopic.Location = new System.Drawing.Point(66, 3);
      this.txtTopic.Name = "txtTopic";
      this.txtTopic.Size = new System.Drawing.Size(350, 27);
      this.txtTopic.TabIndex = 0;
      this.txtTopic.Text = "khoa/test";
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 4;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.03101F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.96899F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
      this.tableLayoutPanel1.Controls.Add(this.txtHostenter, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.txtPortenter, 3, 0);
      this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 48);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(419, 52);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // txtHostenter
      // 
      this.txtHostenter.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.txtHostenter.Location = new System.Drawing.Point(54, 12);
      this.txtHostenter.Name = "txtHostenter";
      this.txtHostenter.Size = new System.Drawing.Size(155, 27);
      this.txtHostenter.TabIndex = 0;
      this.txtHostenter.Text = "192.168.1.10";
      // 
      // txtPortenter
      // 
      this.txtPortenter.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.txtPortenter.Location = new System.Drawing.Point(311, 12);
      this.txtPortenter.Name = "txtPortenter";
      this.txtPortenter.Size = new System.Drawing.Size(105, 27);
      this.txtPortenter.TabIndex = 0;
      this.txtPortenter.Text = "1883";
      // 
      // label1
      // 
      this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(217, 16);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(38, 20);
      this.label1.TabIndex = 5;
      this.label1.Text = "Port:";
      // 
      // label2
      // 
      this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 16);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(43, 20);
      this.label2.TabIndex = 5;
      this.label2.Text = "Host:";
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.HotTrack;
      this.tableLayoutPanel3.ColumnCount = 4;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.61905F));
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.38095F));
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
      this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
      this.tableLayoutPanel3.Controls.Add(this.txtName, 1, 0);
      this.tableLayoutPanel3.Controls.Add(this.label5, 2, 0);
      this.tableLayoutPanel3.Controls.Add(this.comboProject, 3, 0);
      this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 1;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel3.Size = new System.Drawing.Size(419, 39);
      this.tableLayoutPanel3.TabIndex = 1;
      // 
      // label3
      // 
      this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.label3.AutoSize = true;
      this.label3.ForeColor = System.Drawing.Color.White;
      this.label3.Location = new System.Drawing.Point(3, 9);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(52, 20);
      this.label3.TabIndex = 5;
      this.label3.Text = "Name:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txtName
      // 
      this.txtName.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.txtName.Location = new System.Drawing.Point(61, 6);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(146, 27);
      this.txtName.TabIndex = 0;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.ForeColor = System.Drawing.Color.White;
      this.label5.Location = new System.Drawing.Point(213, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(62, 39);
      this.label5.TabIndex = 5;
      this.label5.Text = "Project:";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // comboProject
      // 
      this.comboProject.FormattingEnabled = true;
      this.comboProject.Location = new System.Drawing.Point(281, 4);
      this.comboProject.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.comboProject.Name = "comboProject";
      this.comboProject.Size = new System.Drawing.Size(135, 28);
      this.comboProject.TabIndex = 6;
      // 
      // btnAdd
      // 
      this.btnAdd.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
      this.btnAdd.Location = new System.Drawing.Point(297, 212);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size(137, 35);
      this.btnAdd.TabIndex = 3;
      this.btnAdd.Text = "Add Client";
      this.btnAdd.UseVisualStyleBackColor = false;
      this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Location = new System.Drawing.Point(11, 218);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(94, 29);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnReset
      // 
      this.btnReset.Location = new System.Drawing.Point(123, 218);
      this.btnReset.Name = "btnReset";
      this.btnReset.Size = new System.Drawing.Size(94, 29);
      this.btnReset.TabIndex = 4;
      this.btnReset.Text = "Reset";
      this.btnReset.UseVisualStyleBackColor = true;
      this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(18, 170);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(51, 20);
      this.label7.TabIndex = 5;
      this.label7.Text = "Gmail:";
      // 
      // txtGmail
      // 
      this.txtGmail.Location = new System.Drawing.Point(82, 166);
      this.txtGmail.Name = "txtGmail";
      this.txtGmail.Size = new System.Drawing.Size(351, 27);
      this.txtGmail.TabIndex = 6;
      this.txtGmail.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
      // 
      // formAdding
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(450, 262);
      this.Controls.Add(this.txtGmail);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.btnReset);
      this.Controls.Add(this.btnAdd);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.tableLayoutPanel2);
      this.Name = "formAdding";
      this.Text = "adding";
      this.Load += new System.EventHandler(this.formAdding_Load);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.tableLayoutPanel4.PerformLayout();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.tableLayoutPanel3.ResumeLayout(false);
      this.tableLayoutPanel3.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox txtHostenter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox txtTopic;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPortenter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtGmail;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox comboProject;
  }
}