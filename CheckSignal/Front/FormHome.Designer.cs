﻿
namespace CheckSignal
{
    partial class FormHome
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHome));
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.statusHome = new System.Windows.Forms.Panel();
      this.addButton = new System.Windows.Forms.Panel();
      this.btnClear = new System.Windows.Forms.Panel();
      this.LoadClientButton = new System.Windows.Forms.Button();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.tableLayoutPanel4.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.BackColor = System.Drawing.Color.MediumSeaGreen;
      this.tableLayoutPanel4.ColumnCount = 5;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
      this.tableLayoutPanel4.Controls.Add(this.statusHome, 0, 0);
      this.tableLayoutPanel4.Controls.Add(this.addButton, 3, 0);
      this.tableLayoutPanel4.Controls.Add(this.btnClear, 4, 0);
      this.tableLayoutPanel4.Controls.Add(this.LoadClientButton, 2, 0);
      this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 1;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(1192, 47);
      this.tableLayoutPanel4.TabIndex = 3;
      // 
      // statusHome
      // 
      this.statusHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.statusHome.Location = new System.Drawing.Point(3, 3);
      this.statusHome.Name = "statusHome";
      this.statusHome.Size = new System.Drawing.Size(203, 32);
      this.statusHome.TabIndex = 2;
      // 
      // addButton
      // 
      this.addButton.BackgroundImage = global::CheckSignal.Properties.Resources.add;
      this.addButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.addButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this.addButton.Location = new System.Drawing.Point(938, 3);
      this.addButton.Name = "addButton";
      this.addButton.Size = new System.Drawing.Size(122, 41);
      this.addButton.TabIndex = 2;
      this.addButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.addButton_MouseClick);
      // 
      // btnClear
      // 
      this.btnClear.BackgroundImage = global::CheckSignal.Properties.Resources.clear;
      this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btnClear.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnClear.Location = new System.Drawing.Point(1066, 3);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size(123, 41);
      this.btnClear.TabIndex = 3;
      this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
      // 
      // LoadClientButton
      // 
      this.LoadClientButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.LoadClientButton.BackColor = System.Drawing.Color.DarkRed;
      this.LoadClientButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.LoadClientButton.ForeColor = System.Drawing.Color.White;
      this.LoadClientButton.Image = global::CheckSignal.Properties.Resources.refresh_20px;
      this.LoadClientButton.Location = new System.Drawing.Point(815, 4);
      this.LoadClientButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.LoadClientButton.Name = "LoadClientButton";
      this.LoadClientButton.Size = new System.Drawing.Size(117, 39);
      this.LoadClientButton.TabIndex = 4;
      this.LoadClientButton.Text = "Load Client";
      this.LoadClientButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.LoadClientButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.LoadClientButton.UseVisualStyleBackColor = false;
      this.LoadClientButton.Click += new System.EventHandler(this.LoadClientButton_Click);
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.AutoScroll = true;
      this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 47);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(1192, 462);
      this.flowLayoutPanel1.TabIndex = 4;
      // 
      // FormHome
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1192, 509);
      this.Controls.Add(this.flowLayoutPanel1);
      this.Controls.Add(this.tableLayoutPanel4);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "FormHome";
      this.Text = "MQTT Client Monitor";
      this.Load += new System.EventHandler(this.FormHome_Load);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel btnClear;
        private System.Windows.Forms.Panel addButton;
        private System.Windows.Forms.Panel statusHome;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.Button LoadClientButton;
  }
}

