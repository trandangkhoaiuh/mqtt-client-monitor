﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.IO;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Net;
using CheckSignal.Front;
using CheckSignal.Back.DataAccess;
using CheckSignal.Back;
using Microsoft.EntityFrameworkCore;

namespace CheckSignal
{
  public partial class FormHome : Form
  {
    public FormHome()
    {
      InitializeComponent();
    }

    private List<Factoryinfo> factoryInfos = new List<Factoryinfo>();
    private List<Client> Clients = new List<Client>();
    private int indexx = 0;
    private List<Factoryinfo> items;
    string connec;
    SQLiteProvider SQLite = new SQLiteProvider();

    private void addButton_MouseClick(object sender, MouseEventArgs e)
    {
      formAdding add = new formAdding();
      var dResult = add.ShowDialog();
      if (dResult == DialogResult.OK)
      {
        //var uc = new UcNewFactory();
        addNewClient(add.client);
      }
      //add.Close();
    }
    public async void addNewClient(Client client)
    {
      client = await AddNewClientToDB(client);
      if (client == null)
      {
        MessageBox.Show("Cập nhật thất bại, vui lòng thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      //
      UcNewFactory ucFactory = new UcNewFactory();

      ucFactory.CloseEvent += UcFactory_CloseEvent;
      //ucFactory.factoryInfo = factoryInfos[indexx];

      ucFactory.ClientInfo = client;
      ucFactory.frmParent = this;

      if (flowLayoutPanel1.InvokeRequired)
      {
        flowLayoutPanel1.Invoke((MethodInvoker)delegate ()
        {
          flowLayoutPanel1.Controls.Add(ucFactory);
        });
      }
      else
      {
        flowLayoutPanel1.Controls.Add(ucFactory);
      }
      //indexx++;
      ucFactory.loadDatanew();
    }
    public async void addUCform(int IDD, string namee, string IPP, string topicc, string Projectt, string gmaill)
    {
      var new_c = new Client()
      {

        Name = namee,
        HostIP = IPP,
        Topic = topicc,
        Project = Projectt,
        FactoryId = 1,
        Gmail = gmaill
      };
      bool ret = await AddNewClientToDB(new List<Client>() { new_c });
      if (!ret)
      {
        MessageBox.Show("Cập nhật thất bại, vui lòng thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      LoadConfig();


    }
    private Task<bool> AddNewClientToDB(List<Client> clients)
    {
      return Task<bool>.Run(() =>
      {
        using (var db = new MQTTContext())
        {
          try
          {
            db.Database.BeginTransaction();
            for (int i = 0; i < clients.Count; i++)
            {
              if (db.Client.Where(s => s.Name == clients[i].Name && s.Project == clients[i].Project).FirstOrDefault() != null)
                continue;
              db.Client.Add(clients[i]);
            }
            db.SaveChanges();
            db.Database.CommitTransaction();

            return true;
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex.Message);
            db.Database.RollbackTransaction();
            return false;
          }
        }
      });
    }
    private Task<Client> AddNewClientToDB(Client client)
    {
      return Task<Client>.Run(() =>
      {
        using (var db = new MQTTContext())
        {
          try
          {
            db.Database.BeginTransaction();
            var exist = db.Client.Where(s => s.Name == client.Name && s.Project == client.Project).Include(s => s.Factory).FirstOrDefault();
            if (exist != null)
              return exist;
            db.Client.Add(client);
            db.SaveChanges();
            db.Database.CommitTransaction();

            return db.Client.Where(s => s.Name == client.Name && s.Project == client.Project).Include(s => s.Factory).FirstOrDefault();
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex.Message);
            db.Database.RollbackTransaction();
            return null;
          }
        }
      });
    }
    private void UcFactory_CloseEvent(object sender, int clientID)
    {
      var uc = sender as UcNewFactory;
      flowLayoutPanel1.Controls.Remove(uc);
      uc.Dispose();
    }
    private void FormHome_Load(object sender, EventArgs e)
    {
      var datafile = Application.StartupPath + "DataDB.db";
      connec = SQLite.DbConnectionString = "Data Source= DataDB.db";
      this.WindowState = FormWindowState.Maximized;
      DataAccess.Ins.Init().Wait();
    }
    private void loadconfigButton_MouseClick(object sender, MouseEventArgs e)
    {
      //SqlLoad();
      LoadConfig();
    }
    private async void LoadConfig()
    {
      var noti = new FrmNotify() { StartPosition = FormStartPosition.CenterScreen, TopMost = true, Message = "Đang load config...." };
      noti.Show();
      var ret = await LoadConfigFromDB();
      UpdateConfigUI(ret);
      noti.Close();
    }
    private async void LoadConfig(int id)
    {
      var noti = new FrmNotify() { StartPosition = FormStartPosition.CenterScreen, TopMost = true, Message = "Đang load config...." };
      noti.Show();
      var ret = await LoadConfigFromDB(id);
      UpdateConfigUI(ret);
      noti.Close();
    }
    private Task<List<Client>> LoadConfigFromDB()
    {
      return Task<List<Client>>.Run(() =>
      {
        using (var db = new MQTTContext())
        {
          List<Client> ret = new List<Client>();
          try
          {
            ret = db.Client
            .Include(s => s.Factory)
            .OrderBy(s => s.Project)
            .ToList();
            return ret;
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex.StackTrace);
            return ret;
          }
        }
      });

    }
    private Task<List<Client>> LoadConfigFromDB(int id)
    {
      return Task<List<Client>>.Run(() =>
      {
        using (var db = new MQTTContext())
        {
          List<Client> ret = new List<Client>();
          try
          {
            ret = db.Client.Where(s => s.Id == id).Include(s => s.Factory).ToList();
            return ret;
          }
          catch (Exception ex)
          {

            Console.WriteLine(ex.StackTrace);
            return ret;

          }

        }
      });

    }
    private void UpdateConfigUI(List<Client> clients)
    {
      //Change UI

      factoryInfos.Clear();
      flowLayoutPanel1.Controls.Clear();
      //indexx = 0;
      foreach (var item in clients)
      {


        UcNewFactory ucFactory = new UcNewFactory();
        ucFactory.CloseEvent += UcFactory_CloseEvent;
        //ucFactory.factoryInfo = factoryInfos[indexx];
        ucFactory.ClientInfo = item;
        //ucFactory.Dock = DockStyle.Left;
        //ucFactory.AutoSize = true;
        ucFactory.frmParent = this;

        if (flowLayoutPanel1.InvokeRequired)
        {
          flowLayoutPanel1.Invoke((MethodInvoker)delegate ()
          {

            flowLayoutPanel1.Controls.Add(ucFactory);
          });
        }
        else
        {
          flowLayoutPanel1.Controls.Add(ucFactory);
        }
        //indexx++;
        ucFactory.loadDatanew();

      }
    }
    async void SqlLoad()
    {
      var listItem = new List<Factoryinfo>();
      var noti = new FrmNotify() { StartPosition = FormStartPosition.CenterScreen, TopMost = true, Message = "Đang load config...." };
      noti.Show();
      await Task.Run(() =>
      {
        string cmd = "SELECT * FROM FactoryData ORDER BY Project";
        var TableProject = SQLite.GetDataTable(cmd);

        foreach (var row in TableProject.Rows)
        {
          var row_d = row as DataRow;
          listItem.Add(new Factoryinfo
          {
            Host = row_d.ItemArray[1].ToString(),
            Topic = row_d.ItemArray[2].ToString(),
            FactoryName = row_d.ItemArray[3].ToString(),
            ClientId = Convert.ToInt32(row_d.ItemArray[4]),
            Project = row_d.ItemArray[5].ToString(),
            Gmail = row_d.ItemArray[6].ToString(),
          });
        }
        listItem.OrderByDescending(x => x.Project);

      });
      //Change UI

      factoryInfos.Clear();
      flowLayoutPanel1.Controls.Clear();
      indexx = 0;
      foreach (var item in listItem)
      {
        //System.Diagnostics.Debug.WriteLine(item.Project);

        factoryInfos.Add(new Factoryinfo
        {
          ClientId = item.ClientId,
          FactoryName = item.FactoryName,
          Host = item.Host,
          LastDiss = "...",
          Date = 0,
          LastRecv = "...",
          Topic = item.Topic,
          Index = indexx,
          Project = item.Project,
          Gmail = item.Gmail,
        }); ;

        UcNewFactory ucFactory = new UcNewFactory();
        ucFactory.CloseEvent += UcFactory_CloseEvent;
        ucFactory.factoryInfo = factoryInfos[indexx];
        ucFactory.frmParent = this;

        if (flowLayoutPanel1.InvokeRequired)
        {
          flowLayoutPanel1.Invoke((MethodInvoker)delegate ()
          {

            flowLayoutPanel1.Controls.Add(ucFactory);
          });
        }
        else
        {
          flowLayoutPanel1.Controls.Add(ucFactory);
        }
        indexx++;
        ucFactory.loadDatanew();
        noti.Close();
      }
    }
    void save2sql(string clID, string host, string topic, string factoryname, string project, string gmail)
    {
      Dictionary<string, string> name = new Dictionary<string, string>();
      name.Add("ClientId", clID);
      name.Add("Host", host);
      name.Add("Topic", topic);
      name.Add("FactoryName", factoryname);
      name.Add("Project", project);
      name.Add("Gmail", gmail);
      string table = "FactoryData";
      if (SQLite.CheckTableExists(table))
      {
        if (SQLite.CheckColumnExists(table, "FactoryName"))
        {
          if (SQLite.Insert(table, name))
          {
            System.Diagnostics.Debug.WriteLine("ok");
          }
        }
      }
    }



    private void btnClear_Click(object sender, EventArgs e)
    {
      Front.FormClearClient clear = new Front.FormClearClient();
      //clear.Show();
      var dResult = clear.ShowDialog();
      if (dResult == DialogResult.OK)
      {
        if (clear.delete)
        {
          //SQLite.ClearTable("FactoryData");
          
          var resq = MessageBox.Show("Are you sure you want to Clear all clients?" + Environment.NewLine + "This action will delete all clients in the database!", "Clear All Client in DataBase", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (resq == DialogResult.Yes)
          {
            DeleteDB(0);
          }
        }
        foreach (var control in flowLayoutPanel1.Controls)
        {
          (control as Control)?.Dispose();
        }
        flowLayoutPanel1.Controls.Clear();
        indexx = 0;
      }
    }
    private Task<bool> DeleteDB(int id)
    {
      return Task<bool>.Run(() =>
      {
        using (var db = new MQTTContext())
        {
          try
          {
            db.Database.BeginTransaction();
            if (id != 0)
            {
              var exist = db.Client.Find(id);
              if (exist != null)
                db.Client.Remove(exist);
            }
            else
            {
              db.Client.RemoveRange(db.Client.ToArray());
            }
            db.SaveChanges();
            db.Database.CommitTransaction();
            return true;

          }
          catch (Exception ex)
          {
            db.Database.RollbackTransaction();
            Console.WriteLine(ex.StackTrace);
            return false;
          }
        }
      });
    }
    private void LoadClientButton_Click(object sender, EventArgs e)
    {
      LoadConfig();
    }
  }
}
