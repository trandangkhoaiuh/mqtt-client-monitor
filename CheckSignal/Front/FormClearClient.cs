﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckSignal.Front
{
    public partial class FormClearClient : Form
    {
        public bool delete=false;
        public FormClearClient()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(checkBoxDelete.Checked && !checkBoxClear.Checked)
            {
                delete = true;
                this.DialogResult = DialogResult.OK;
            }
            else if(!checkBoxDelete.Checked && checkBoxClear.Checked)
            {
                delete = false;
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Please select an Option");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
