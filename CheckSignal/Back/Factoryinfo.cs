﻿
namespace CheckSignal
{
    public class Factoryinfo
    {
        public string FactoryName { get; set; }
        public string ClientName { get; set; }
        public string Host { get; set; }
        public int ClientId { get; set; }
        public double Date { get; set; }
        public string LastRecv { get; set; }
        public string LastDiss  { get; set; }
        public string Topic { get; set; }
        public int ConnStatus { get; set; }
        public int Index { get; set; }
        public string Message { get; set; }
        public string Project { get; set; }
        public string Gmail { get; set; }

        //{"ClientId":25,"ClientName":"Khoa1","Date":1635146621,"ConnStatus":1,"Message":"AbcXyz"}


    }
}
