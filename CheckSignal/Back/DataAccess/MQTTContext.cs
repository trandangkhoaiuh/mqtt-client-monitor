﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckSignal.Back.DataAccess
{
  public class MQTTContext : DbContext
  {
    public DbSet<Factory> Factory { get; set; }
    public DbSet<Client> Client { get; set; }
    private string path = Application.StartupPath;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        optionsBuilder.UseSqlite($"Data Source = {path}\\DataDB.db;");
      }
    }
  }
}
