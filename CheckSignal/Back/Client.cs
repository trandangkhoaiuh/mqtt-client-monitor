﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSignal.Back
{
  [Table("Client")]
  public class Client
  {
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public int FactoryId { get; set; }
    public Factory Factory { get; set; }
    public string Name { get; set; }
    public string HostIP { get; set; }
    public string HostPort { get; set; }

    public double Date { get; set; }
    public string LastRecv { get; set; }
    public string LastDiss { get; set; }
    public string Topic { get; set; }
    public int ConnStatus { get; set; }
    public int Index { get; set; }
    public string Message { get; set; }
    public string Project { get; set; }
    public string Gmail { get; set; }
  }
}
